class UserMailer < ActionMailer::Base
  default from: "david@dapperchef.com"

  def new_user(user)
    @user = user
    mail(to: admin, subject: 'We have a new user, yay!')
  end

  def welcome_email(user)
    @user = user
    email_with_name = "#{@user.first_name} #{@user.last_name} <#{@user.email}>"
    @url = 'http://dapperchef.com/login'
    mail(to: email_with_name, subject: 'Welcome to DapperChef.com')
    UserMailer.new_user(@user).deliver
  end

  def admin
    @admin = ENV["ADMIN"]
  end

end
