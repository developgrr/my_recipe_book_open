json.array!(@recipes) do |recipe|
  json.extract! recipe, :id, :name, :directions, :belongs_to, :image
  json.url recipe_url(recipe, format: :json)
end
