class User < ActiveRecord::Base

  validates_presence_of :email
  validates_uniqueness_of :email

  has_many :recipes, dependent: :destroy
  has_many :collections, dependent: :destroy
  has_and_belongs_to_many :friends,
      class_name: "User",
      foreign_key: "this_user_id",
      association_foreign_key: "other_user_id"

  mount_uploader :image, ImageUploader

  after_create 'self.welcome_user'
  before_destroy 'self.clear_friends'

  def self.from_omniauth(auth)
    create! do |user|  
      user.provider = auth.provider
      user.uid = auth.uid
      user.email = auth.info.email
      user.first_name = auth.info.first_name
      user.last_name = auth.info.last_name
      user.remote_image_url = auth.info.image.gsub('http://','https://')
      user.facebook_token = auth.credentials.token
    end
  end

  def welcome_user
    UserMailer.welcome_email(self).deliver
  end

  def profile_image
    self.image.versions[:web].preview.url
  end

  def list_image
    self.image.versions[:web].thumb.url
  end

  def post(recipe, url)
    content = "This recipe for #{recipe.name} looks amazing. Check it out at #{url}"
    graph.put_wall_post(content)
  end

  def add_friend(user)
    self.friends << user unless self.friends.exists? user
    user.friends << self unless user.friends.exists? self
  end

  def clear_friends
    self.friends.each do |friend|
      delete_friend(friend)
    end
  end

  def delete_friend(friend)
    self.friends.delete friend
    friend.friends.delete self
  end

  private

  def graph
    @graph = Koala::Facebook::API.new(self.facebook_token)
  end

end
