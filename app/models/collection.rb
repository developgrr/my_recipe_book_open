class Collection < ActiveRecord::Base
  has_and_belongs_to_many :recipes
  belongs_to :user

  after_find do |collection|
    collection.count_of_recipes = collection.recipes.count
    collection.save!
  end

  def add_recipe(recipe)
    unless self.recipes.exists? recipe
      if self.user.recipes.exists? recipe
        new_recipe = recipe
      else
        new_recipe = self.user.recipes.build
        new_recipe.name = recipe.name
        new_recipe.ingredients = recipe.ingredients
        new_recipe.directions = recipe.directions
        new_recipe.image = recipe.image
        new_recipe.save!
        new_recipe.reload
      end
      self.recipes << new_recipe
      return true
    end
    return false
  end

  def remove_recipe(recipe)
    if self.recipes.exists? recipe then self.recipes.delete recipe end
  end

end
