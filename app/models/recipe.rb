class Recipe < ActiveRecord::Base

  validates_presence_of :name, :ingredients, :directions

  belongs_to :user
  has_and_belongs_to_many :collections

  mount_uploader :image, ImageUploader

  def list_image
    self.image.versions[:web].preview.url
  end

  def ingredients_list
    ings = self.ingredients.scan(/.*$/)
    ings.keep_if {|x|x.present?}
    ings.map do |i|
      if i.include? "\r"
        i.gsub!("\r", "")
      end
    end
    ings
  end

end
