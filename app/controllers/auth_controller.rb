class AuthController < ApplicationController
  skip_filter :ensure_logged_in
  def facebook
    @user = User.from_omniauth(auth_hash)
    self.current_user = @user
  end

  protected

  def auth_hash
    request.env['omniauth.auth']
  end

end