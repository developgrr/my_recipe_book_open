class SessionsController < ApplicationController
  
  skip_filter :ensure_logged_in


  def new
  end

  def create  
    auth = request.env["omniauth.auth"]
    user = User.find_by_provider_and_uid(auth.provider, auth.uid) || User.from_omniauth(auth)
    session[:user_id] = user.id
    redirect_to root_url, :notice => "You've signed in with Facebook!"
  end

  def destroy
    session[:user_id] = nil
    redirect_to login_path, notice: "You logged out!"
  end

  private

end
