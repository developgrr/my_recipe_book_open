class WelcomeController < ApplicationController

  skip_filter :ensure_logged_in, only: [:about, :help]
  
  def index
  end
end
