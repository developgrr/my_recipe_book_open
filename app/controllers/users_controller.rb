class UsersController < ApplicationController
  skip_filter :ensure_logged_in, only: [:new, :create]
  before_action :set_user, only: [:edit, :update, :destroy]

  # GET /users
  # GET /users.json
  def index
    unless current_user.admin == true
      redirect_to :back, notice: "Only admins can do that :)."
    end
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
    user_id = params[:id]
    @user = User.find(user_id)
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save

        UserMailer.welcome_email(@user).deliver
        format.html { redirect_to login_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'User was successfully updated.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def facebook_share
    recipe_id = params[:recipe_id]
    recipe = Recipe.find(recipe_id)
    if check_permissions
      current_user.post(recipe, recipe_url(recipe))
      redirect_to :back, notice: "You've shared this recipe!"
    else
      redirect_to :back, notice: "Oops, we don't have permission to post this for you. :("
    end
  end

  def add_friend
    new_friend = User.find(params[:friend_id])
    current_user.add_friend(new_friend)
    redirect_to :back, notice: "User was successfully added to friends."
  end

  def remove_friend
    friend = User.find(params[:friend_id])
    current_user.delete_friend(friend)
    redirect_to :back, notice: "User was successfully removed from friends."
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(current_user.id)
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:first_name, :last_name, :email, :password, :password_confirmation, :image)
    end

    def content(recipe)
      "#{recipe_path(recipe)}"
    end

    def check_permissions
      permissions = graph.get_object("me/permissions")
      status = permissions.select { |perm_hash| perm_hash["permission"] == "publish_actions" }.first
      if status == nil || status["status"] != "granted"
        return false
      else
        return true
      end
    end

    def graph
      @graph = Koala::Facebook::API.new(current_user.facebook_token)
    end
end
