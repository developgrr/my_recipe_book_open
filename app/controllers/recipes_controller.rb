class RecipesController < ApplicationController
  before_action :set_recipe, only: [:show, :edit, :update, :destroy, :add_recipe, :remove_recipe]

  before_filter only: [:edit, :update, :destroy] do |c|
    c.send(:allowed?, @recipe)
  end

  # GET /recipes
  # GET /recipes.json
  def index
    search_q = params[:q]
    @q = Recipe.search(search_q)
    @recipes = Recipe.all
  end

  def search
    search_q = params[:q]
    @q = Recipe.search(search_q)
    @recipes = @q.result(distinct: true)
    render :index
  end

  # GET /recipes/1
  # GET /recipes/1.json
  def show
  end

  # GET /recipes/new
  def new
    @recipe = Recipe.new
  end

  # GET /recipes/1/edit
  def edit
  end

  # POST /recipes
  # POST /recipes.json
  def create
    @recipe = Recipe.new(recipe_params)

    respond_to do |format|
      if @recipe.save
        format.html { redirect_to @recipe, notice: 'Recipe was successfully created.' }
        format.json { render :show, status: :created, location: @recipe }
      else
        format.html { render :new }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /recipes/1
  # PATCH/PUT /recipes/1.json
  def update
    respond_to do |format|
      if @recipe.update(recipe_params)
        format.html { redirect_to @recipe, notice: 'Recipe was successfully updated.' }
        format.json { render :show, status: :ok, location: @recipe }
      else
        format.html { render :edit }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /recipes/1
  # DELETE /recipes/1.json
  def destroy
    @recipe.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Recipe was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def add_recipe
    collection = Collection.find(params[:collection_id])
    
    respond_to do |format|
      if collection.add_recipe(@recipe)
        format.html { redirect_to :back, notice: 'Recipe was added to collection.' }
        format.json { render :show, status: :ok, location: @recipe }
      else
        format.html { render :back, notice: 'Oops, we couldn\'t add that one. :(' }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  def remove_recipe
    collection = Collection.find(params[:collection_id])
    collection.remove_recipe(@recipe)
    respond_to do |format|
      if !collection.recipes.exists? @recipe
        format.html { redirect_to :back, notice: 'Recipe was removed from collection.' }
        format.json { render :show, status: :ok, location: @recipe }
      else
        format.html { render :back, notice: 'Oops, we couldn\'t find or remove that one. :(' }
        format.json { render json: @recipe.errors, status: :unprocessable_entity }
      end
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_recipe
      @recipe = Recipe.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def recipe_params
      params.require(:recipe).permit(:name, :directions, :user_id, :image, :ingredients)
    end
end
