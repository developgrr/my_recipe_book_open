Rails.application.config.middleware.use OmniAuth::Builder do
  facebook_key = ENV["FACEBOOK_KEY"]
  facebook_secret= ENV["FACEBOOK_SECRET"]
  provider :facebook, facebook_key, facebook_secret, scope: "email, user_friends", :image_size => { :width => "200", :height => "200" }
end