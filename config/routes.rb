Rails.application.routes.draw do
  
  resources :collections

  root to: "welcome#index"
  get 'help' => "welcome#help"
  get 'about' => "welcome#about"

  resources :recipes do
    member do
      post :add_recipe
      post :remove_recipe
    end

    collection do
      post :search
    end
  end

  get 'register' => 'users#new'
  resources :users do
    member do
      post :add_friend
      post :remove_friend
    end
  end
  post 'facebook/share' => 'users#facebook_share'

  get 'logout' => 'sessions#destroy'
  get 'login' => 'sessions#new'
  get '/auth/facebook' => 'auth#facebook'
  get 'auth/facebook/callback' => 'sessions#create'
  get 'auth/facebook/rerequest' => 'auth#rerequest_facebook'

  resources :sessions, only: [:new, :create, :destroy]
  
end
