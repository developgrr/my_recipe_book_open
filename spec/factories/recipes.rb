# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :recipe do
    name "MyString"
    directions "MyText"
    belongs_to ""
    image "MyString"
  end
end
