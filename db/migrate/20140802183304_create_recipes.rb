class CreateRecipes < ActiveRecord::Migration
  def change
    create_table :recipes do |t|
      t.string :name
      t.text :directions
      t.integer :user_id
      t.string :image

      t.timestamps
    end
  end
end
