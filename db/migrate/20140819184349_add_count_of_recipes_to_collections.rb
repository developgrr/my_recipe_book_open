class AddCountOfRecipesToCollections < ActiveRecord::Migration
  def change
    add_column :collections, :count_of_recipes, :integer
  end
end
