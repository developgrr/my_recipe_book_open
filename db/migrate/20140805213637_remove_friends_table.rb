class RemoveFriendsTable < ActiveRecord::Migration
  def change
    drop_table :friends_users

    create_table :friends_users do |t|
      t.integer :user_id
      t.integer :friend_id

      t.index [:friend_id, :user_id]
      t.index [:user_id, :friend_id]
    end
  end
end
