class ChangeFriends < ActiveRecord::Migration
  def change
    drop_table :friends_users

    create_table :friends_users do |t|
      t.integer :friend_id
      t.integer :user_id
    end

    add_index :friends_users, [:friend_id, :user_id]
    add_index :friends_users, [:user_id, :friend_id]
  end
end
