class CreateCollections < ActiveRecord::Migration
  def change
    create_table :collections do |t|
      t.string :name
      t.timestamps
    end
    create_join_table :collections, :recipes do |t|
      t.index [:collection_id, :recipe_id]
      t.index [:recipe_id, :collection_id]
    end
  end
end
