class CreateUserUserJoinTable < ActiveRecord::Migration
  def change
    create_table :users_users do |t|
      t.integer :this_user_id
      t.integer :other_user_id
    end
  end
end
