# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140820172426) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "collections", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "count_of_recipes"
  end

  create_table "collections_recipes", id: false, force: true do |t|
    t.integer "collection_id", null: false
    t.integer "recipe_id",     null: false
  end

  add_index "collections_recipes", ["collection_id", "recipe_id"], name: "index_collections_recipes_on_collection_id_and_recipe_id", using: :btree
  add_index "collections_recipes", ["recipe_id", "collection_id"], name: "index_collections_recipes_on_recipe_id_and_collection_id", using: :btree

  create_table "recipes", force: true do |t|
    t.string   "name"
    t.text     "directions"
    t.integer  "user_id"
    t.string   "image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "ingredients"
  end

  create_table "users", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "image"
    t.string   "facebook_token"
    t.string   "provider"
    t.string   "uid"
    t.boolean  "admin",           default: false
  end

  create_table "users_users", force: true do |t|
    t.integer "this_user_id"
    t.integer "other_user_id"
  end

end
